#!/usr/bin/env python3
import gitlab

SRC = {
    # put here source repository parameters
    # token is required if the source repository is private
    # source repository won't be modified by this script in any way
    'url': 'https://git.unistra.fr',
    'path': 'estrades/heimdall',
    'token': 'glpat-1Q1337WVZdeadbeefyz',
  }
# if you don't want some issues from the source repository, put 'em here
SKIP_ISSUES = [ 4 ]
DST = {
    # put here destination repository parameters
    # token serves no purpose, as the token used are in DST_TOKEN (see below)
    'url': 'https://gitlab.huma-num.fr',
    'path': 'datasphere/heimdall',
    'token': None,
  }

DST_TOKENS = {
    # put, as a key, usernames of all members of the destination repository
    # put, as key values, each username access token ; this is needed because
    #   the GitLab api doesn't allow you to say "this guy/gal has said this"
    #   if you're not connected as this guy/gal, or if you're not admin of
    #   this GitLab instance.
    'rwitz': 'glpat-snfwDDsff121df2dfdfd',
    'gporte': 'glpat-dJxoig49JSbs2544xdyj',
    'e.falcoz': 'glpat-trololo-wazzaaaaaapp',
  }
# 'ME' is your username, as you will close issues & other finishing touches
ME = 'rwitz'
# set 'CREATE_ISSUES' to False if issues already exist in destination repository
CREATE_ISSUES = True
SRC_VS_DST_USER_IDS = {
    # put here the iid of each member of the source:destination repoitory
    # get'em at url/api/v4/users?username=gporte
    1117: 728,  # rwitz
    1861: 263,  # gporte
    4655: 1682,  # e.falcoz:evankote
  }

def get_project(instance, path):
  parts = path.split('/')
  group = None
  project = None
  for index, part in enumerate(parts):
    if index == len(parts)-1:
      project = get_group_project(instance, group, part)
    else:
      group = get_group(instance, part)
      '''
      print("group: %s" % group.path)
      projects = group.projects.list(iterator=True)
      for p in projects:
        print("  subproject: %s" % p.path)
      '''
  return project

def get_group(instance, path):
  groups = instance.groups.list(iterator=True)
  return [g for g in groups if g.path == path][0] 

def get_group_project(instance, group, path):
  projects = group.projects.list(iterator=True)
  project = [p for p in projects if p.path == path][0]
  return instance.projects.get(project.id)

def get_issue_payload(issue):
  update(issue, 'description')
  return {
      'title': issue.title,
      'description': issue.description,
      'created_at': issue.created_at,
      'closed_at': issue.closed_at,
      'confidential': issue.confidential,
      'due_date': issue.due_date,
      'issue_type': issue.issue_type,
    }

def get_note_payload(note):
  update(note, 'body')
  return {
      'system': note.system,
      'body': note.body,
      'created_at': note.created_at,
      'updated_at': note.updated_at,
      'confidential': note.confidential,
      'internal': note.internal,
      'resolvable': note.resolvable,
      'type': note.type,
    }


def get_project_issue(project, iid):
  return project.issues.get(iid)

def get_issue_notes(instance, issue):
  project = instance.projects.get(issue.project_id, lazy=True)
  project_issue = project.issues.get(issue.iid, lazy=True)
  notes = project_issue.notes.list(iterator=True)
  return notes

def update(o, attr):
  old = getattr(o, attr)
  now = old.replace('@e.falcoz', '@evankote')
  setattr(o, attr, now)



if __name__ == '__main__':
  # we'll take stuff from here
  src = gitlab.Gitlab(SRC['url'], private_token=SRC['token'])
  src_project = get_project(src, SRC['path'])

  src_issues = src_project.issues.list(iterator=True)
  for issue_old in reversed(list(src_issues)):
    if CREATE_ISSUES and (issue_old.iid in SKIP_ISSUES):
      print("%s SKIPPED: %s" % (issue_old.iid, issue_old.title))
      continue
    src_author = issue_old.author['username']
    # RECONNECT according to original issue author
    dst = gitlab.Gitlab(DST['url'], private_token=DST_TOKENS[src_author])
    dst.auth()
    dst_project = get_project(dst, DST['path'])
    if CREATE_ISSUES:
      issue_now = dst_project.issues.create(get_issue_payload(issue_old))
      print("%s>%s @%s: %s" % (issue_old.iid, issue_now.iid, src_author, issue_old.title))

    # MIGRATE NOTES
    notes = get_issue_notes(src, issue_old)
    for note in reversed(list(notes)):
      if (note.system):
        continue
      src_author = note.author['username']
      # RECONNECT according to original note author
      dst = gitlab.Gitlab(DST['url'], private_token=DST_TOKENS[src_author])
      dst.auth()
      dst_project = get_project(dst, DST['path'])
      # RETRIEVE ISSUE as original note author
      issue_now = get_project_issue(dst_project, issue_old.iid)
      print("--- [%s] %s @%s" % (('S' if note.system else 'U'), note.created_at, note.author['username']))
      issue_now.notes.create(get_note_payload(note))


    # RECONNECT as me to do the finishing touches
    dst = gitlab.Gitlab(DST['url'], private_token=DST_TOKENS[ME])
    dst.auth()
    dst_project = get_project(dst, DST['path'])
    # RETRIEVE ISSUE as me
    issue_now = get_project_issue(dst_project, issue_old.iid)
    if not CREATE_ISSUES:
      print("%s (%s): %s" % (issue_now.iid, issue_now.author['username'], issue_now.title))
    issue_now.milestone_id = issue_old.milestone['iid'] if issue_old.milestone else None
    issue_now.labels = issue_old.labels
    if issue_old.assignee:
      issue_now.assignee_id = SRC_VS_DST_USER_IDS[issue_old.assignee['id']]
      issue_now.assignee_ids = [ SRC_VS_DST_USER_IDS[issue_old.assignee['id']] ]
    if issue_old.state == 'closed':
      issue_now.state_event = 'close'
    issue_now.save()
